/****************************************************************************
 * Copyright (C) 2020 by Tamas Szabo                                        *
 *                                                                          *
 * This file is part of a school project.                                   *
 ****************************************************************************/

#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "lib.h"

/** @brief Removes spaces, (, ), -, / characters from a string
 *
 * @param input the string which will be filtered
 * @return the filtered string
 */
char *simplify_text(char *input) {
    if (input == NULL) {
        return input;
    }
    int len = strlen(input);
    // Allocate the input character's length. The output string cannot be larger than the input, so we'll be fine.
    char *output = (char*)malloc(len * sizeof(output));

    int output_point = 0;
    for (int i=0; i < len; i++) {
        bool is_filtered = false;
        for (int u=0;u<5;u++) {
            if (input[i] == ACCEPTED_CHARACTERS[u]) {
                is_filtered = true;
                u = 5; // exit inner loop. n00b solution, but it works. :D
            }
        }

        // Does not append the allowed character.
        if (!is_filtered) {
            output[output_point++] = input[i];
        }
    }

    // Terminate the string
    output[output_point] = '\0';

    return output;
}

/** @brief Checks whether the string has brackets, and they're paired.
 *
 * Limitations: The function does not check the order of brackets, only count them.
 *
 * @param input string which will be checked
 * @return bool true if paired, false if not
 */
bool is_paired_brackets(char *input) {
    if (input == NULL) {
        return true; //Brackets are valid in null :P
    }
    int current_depth = 0;

    for (int i=0; input[i] != '\0'; i++) {
        // Raise depth if starting a bracket.
        if (input[i] == '(') {
            current_depth ++;
        } // Reduce the dept if closing a bracket.
        else if (input[i] == ')') {
            current_depth --;
        }
    }

    return current_depth == 0;
}

/** @brief Tells about a string if its a phone number or not
 *
 * @param input string which will be checked
 * @return PhoneNumberResult
 */
PhoneNumberResult check_phone_number(char *input) {
    if (input == NULL) {
        return INVALID_CHARACTERS;
    }

    // Check if any string given
    int len = strlen(input);
    if (len == 0) {
        return TOO_SHORT;
    }
    else if(len > MAX_PHONENUMBER_LENGTH) {
        return TOO_LONG;
    }

    // First check if brackets are ok
    bool are_brackets_ok = is_paired_brackets(input);
    if (are_brackets_ok) {
        // simplify the string (remove allowed characters of it)
        char *simplified = simplify_text(input);

        // Check if the remained string is a number or not.
        return is_numeric(simplified) ? VALID : INVALID_CHARACTERS;
    }
    else {
        return BAD_BRACKETS;
    }
}


/** @brief Determines if a string consist of numbers
 *
 * @param input string which will be checked
 * @return bool true if its a number, false if not
 */
bool is_numeric (char *input) {
    if (input == NULL) {
        return false;
    }
    bool all_num = true;

    for (int i=0; input[i] != '\0'; i++) {
        // Check ascii code of the character. 0 is ASCII 48 9 is ASCII 57, if we're in the range, then cool.
        if (input[i] < 48 || input[i] > 57) {
            all_num = false;
            break;
        }
    }

    return all_num;
}
