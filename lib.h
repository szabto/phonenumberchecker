#include <stdbool.h>

#ifndef LIB_H_INCLUDED
#define LIB_H_INCLUDED

// Set the max length of a phone number. See: https://en.wikipedia.org/wiki/Telephone_numbering_plan
#ifndef MAX_PHONENUMBER_LENGTH
#define MAX_PHONENUMBER_LENGTH 15
#endif // MAX_PHONENUMBER_LENGTH

// These characters are allowed in a phone number
#ifndef ACCEPTED_CHARACTERS
#define ACCEPTED_CHARACTERS " ()-/"
#endif // ACCEPTED_CHARACTERS

typedef enum {
    VALID,
    TOO_SHORT,
    TOO_LONG,
    INVALID_CHARACTERS,
    BAD_BRACKETS
} PhoneNumberResult;

bool                is_numeric             (char *input);
PhoneNumberResult   check_phone_number     (char *input);
char                *simplify_text         (char *input);
bool                is_paired_brackets     (char *input);

#endif // LIB_H_INCLUDED
