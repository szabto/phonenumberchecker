/****************************************************************************
 * Copyright (C) 2020 by Tamas Szabo                                        *
 *                                                                          *
 * This file is part of a school project.                                   *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
#include <stdbool.h>
#include <string.h>
#define HELP_ARG "-help"

#define MSG_VALID "Valid phone number"
#define MSG_BAD_BRACKETS "The given number has invalid brackets"
#define MSG_SHORT "The given number is too short (min 1)"
#define MSG_LONG "The given number is too long (max 15)"
#define MSG_INVALID_CHARACTERS "The entered string is not numerical"
#define MSG_DEF_ERROR "Invalid phone number"

char *get_result_string(PhoneNumberResult result) {
    char* ret_string;
    switch(result) {
        case VALID:
            ret_string = MSG_VALID;
        break;
        case BAD_BRACKETS:
            ret_string = MSG_BAD_BRACKETS;
        break;
        case TOO_SHORT:
            ret_string = MSG_SHORT;
        break;
        case TOO_LONG:
            ret_string = MSG_LONG;
        break;
        case INVALID_CHARACTERS:
            ret_string = MSG_INVALID_CHARACTERS;
        break;
        default:
            ret_string = MSG_DEF_ERROR;
        break;
    }

    return ret_string;
}

int main(int argc, char** argv) {
    // Check if arg count is correct, or help requested.
    // I know this is not the best solution for it, but its quick..
    if (argc != 2 || (argc == 2 && strcmp(argv[1], HELP_ARG) == 0)) {
        printf("Usage: phonenumberchecker [phonenumber]\n");
        return 0;
    }

    char* phone_number = argv[1];

    PhoneNumberResult result = check_phone_number(phone_number);

    printf("%s\n", get_result_string(result));

    return 0;
}
